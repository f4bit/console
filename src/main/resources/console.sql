/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50636
 Source Host           : localhost
 Source Database       : console

 Target Server Type    : MySQL
 Target Server Version : 50636
 File Encoding         : utf-8

 Date: 07/05/2017 13:13:42 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `FUNC_INFO`
-- ----------------------------
DROP TABLE IF EXISTS `FUNC_INFO`;
CREATE TABLE `FUNC_INFO` (
  `FUNC_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '功能ID',
  `FUNC_NAME` varchar(32) NOT NULL COMMENT '功能类型',
  `FUNC_DESC` varchar(64) DEFAULT NULL COMMENT '功能描述',
  `TYPE` int(1) NOT NULL COMMENT '功能类型 0-菜单 1-按钮 2-其它',
  `MENU_LEVEL` int(1) DEFAULT NULL COMMENT '菜单级别 1：一级  2：二级 3：三级 4：四级',
  `MENU_ORDER` int(2) DEFAULT NULL COMMENT '菜单顺序',
  `FUNC_ICON` varchar(64) DEFAULT NULL COMMENT '功能显示图标',
  `IS_LEAF` int(1) DEFAULT NULL COMMENT '是否叶子节点 0-否 1-是',
  `FUC_URL` varchar(128) NOT NULL COMMENT '功能URL',
  `PARENT_FUNC_ID` int(11) NOT NULL COMMENT '父级功能ID',
  `STAT` int(1) NOT NULL COMMENT '状态 0-关闭 1-正常',
  `LAST_UPDATE_OPERATOR` varchar(32) NOT NULL COMMENT '记录最近更新人',
  `UPDATE_TIME` char(14) NOT NULL COMMENT '更新时间',
  `CREATE_TIME` char(14) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `OPERATOR_INFO`
-- ----------------------------
DROP TABLE IF EXISTS `OPERATOR_INFO`;
CREATE TABLE `OPERATOR_INFO` (
  `OPERATOR_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '操作员ID',
  `OPERATOR_NAME` varchar(32) NOT NULL COMMENT '操作员名称',
  `OPERATOR_DESC` varchar(64) DEFAULT NULL COMMENT '操作员描述',
  `OPERATOR_LOGIN_ID` varchar(32) NOT NULL COMMENT '操作员登录账号',
  `OPERATOR_LOGIN_PWD` char(64) NOT NULL COMMENT '操作员密码',
  `STAT` int(1) NOT NULL COMMENT '状态 0-关闭 1-正常 2-锁定',
  `FORCED_UPDATE_PWD_STAT` int(1) NOT NULL COMMENT '强制更新密码 0-否 1-是',
  `LAST_LOGIN_IP` varchar(32) DEFAULT NULL COMMENT '最近登录的IP',
  `LOGIN_FAIL_COUNT` int(2) NOT NULL COMMENT '登录错误次数',
  `LAST_UPDATE_OPERATOR` varchar(32) NOT NULL COMMENT '最近记录更新人',
  `UPDATE_TIME` char(14) NOT NULL COMMENT '更新时间',
  `CREATE_TIME` char(14) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`OPERATOR_ID`),
  UNIQUE KEY `OPERATOR_INFO_UN1` (`OPERATOR_LOGIN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `OPERATOR_ROLE_REL_INFO`
-- ----------------------------
DROP TABLE IF EXISTS `OPERATOR_ROLE_REL_INFO`;
CREATE TABLE `OPERATOR_ROLE_REL_INFO` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水号',
  `OPERATOR_ID` int(11) NOT NULL COMMENT '操作员ID',
  `ROLE_ID` int(11) NOT NULL COMMENT '角色ID',
  `STAT` int(1) NOT NULL COMMENT '状态 0-删除 1-正常',
  `LAST_UPDATE_OPERATOR` varchar(32) NOT NULL COMMENT '最近更新人',
  `UPDATE_TIME` char(14) NOT NULL COMMENT '更新时间',
  `CREATE_TIME` char(14) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `OPERATOR_ROLE_REL_INFO_UN1` (`OPERATOR_ID`,`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ROLE_FUNC_REL_INFO`
-- ----------------------------
DROP TABLE IF EXISTS `ROLE_FUNC_REL_INFO`;
CREATE TABLE `ROLE_FUNC_REL_INFO` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水号',
  `ROLE_ID` int(11) NOT NULL COMMENT '角色ID',
  `FUNC_ID` int(11) NOT NULL COMMENT '功能ID',
  `STAT` int(1) NOT NULL COMMENT '状态 0-删除 1-正常',
  `LAST_UPDATE_OPERATOR` varchar(32) NOT NULL COMMENT '最近记录更新人',
  `UPDATE_TIME` char(14) NOT NULL COMMENT '更新时间',
  `CREATE_TIME` char(14) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ROLE_FUNC_REL_INFO_UN1` (`ROLE_ID`,`FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ROLE_INFO`
-- ----------------------------
DROP TABLE IF EXISTS `ROLE_INFO`;
CREATE TABLE `ROLE_INFO` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `ROLE_NAME` varchar(32) NOT NULL COMMENT '角色名称',
  `ROLE_DESC` varchar(64) DEFAULT NULL COMMENT '角色描述',
  `STAT` int(1) NOT NULL COMMENT '状态 0-关闭 1-正常',
  `LAST_UPDATE_OPERATOR` varchar(32) NOT NULL COMMENT '最近记录更新人',
  `UPDATE_TIME` char(14) NOT NULL COMMENT '更新时间',
  `CREATE_TIME` char(14) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
