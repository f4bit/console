package com.fly.console.dao;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
public class OperatorCustInfo implements Serializable {


    private static final long serialVersionUID = -9050871064100082200L;
    private Integer operatorId;

    private String operatorName;

    private String operatorDesc;

    private String merLoginId;

    private String operatorLoginId;

    private String operatorLoginPwd;

    private Integer stat;

    private Integer forcedUpdatePwdStat;

    private String lastLoginIp;

    private Integer loginFailCount;

    private String lastUpdateOperator;

    private String updateTime;

    private String createTime;

    private Set<RoleCustInfo> roleCustInfoSet = new HashSet<>();


}