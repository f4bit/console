package com.fly.console.dao;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Setter
@Getter
public class RoleCustInfo implements Serializable {

    private static final long serialVersionUID = -7635171383136016077L;
    private Integer roleId;

    private String roleName;

    private String roleDesc;

    private Integer stat;

    private String lastUpdateOperator;

    private String updateTime;

    private String createTime;

    private Set<FuncInfo> funcInfoSet = new HashSet<>();

}