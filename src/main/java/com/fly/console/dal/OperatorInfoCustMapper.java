package com.fly.console.dal;


import com.fly.console.dao.FuncInfo;
import com.fly.console.dao.OperatorCustInfo;

import java.util.List;
import java.util.Map;

public interface OperatorInfoCustMapper {

    OperatorCustInfo findOperatorCustInfoByMerAndOperatorLoginId(Map<String,Object> map);

    List<FuncInfo> queryMenuByOperatorId(Map<String,Object> map);
}