package com.fly.console.dal;

import com.fly.console.service.bo.RoleInfoBO;

import java.util.List;

public interface RoleInfoCustMapper {
    List<RoleInfoBO> queryRoleListWithSelected(int operatorId);
}