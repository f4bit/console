package com.fly.console.dal;

import com.fly.console.service.bo.FuncInfoBO;

import java.util.List;

public interface FuncInfoCustMapper {
    List<FuncInfoBO> queryFuncListWithSelected(int roleId);
}