package com.fly.console.controller.system.auth;

import com.fly.console.dao.OperatorInfo;
import com.fly.console.dao.RoleInfo;
import com.fly.console.service.RoleFuncService;
import com.fly.console.service.RoleInfoService;
import com.fly.console.service.bo.RoleFuncBO;
import com.fly.console.service.bo.RoleInfoBO;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by APPLE on 2017/7/12.
 */
@Controller
@RequestMapping("/system/auth/role_manager")
public class RoleController {

    @Autowired
    private RoleInfoService roleInfoService;

    @Autowired
    private RoleFuncService roleFuncService;

    @RequestMapping
    public String roleManager(HttpServletRequest request) {
        return "/system/auth/role";
    }

    @RequestMapping(value = "/roles")
    @ResponseBody
    public Map role(RoleInfo roleInfo, String draw,
                    @RequestParam(required = false, defaultValue = "1") int start,
                    @RequestParam(required = false, defaultValue = "10") int length) {
        Map<String,Object> map = new HashMap<>();
        PageInfo<RoleInfo> pageInfo = roleInfoService.selectByPage(roleInfo, start, length);
        System.out.println("pageInfo.getTotal():"+pageInfo.getTotal());
        map.put("draw",draw);
        map.put("recordsTotal",pageInfo.getTotal());
        map.put("recordsFiltered",pageInfo.getTotal());
        map.put("data", pageInfo.getList());
        return map;
    }

    //分配角色
    @RequestMapping("/saveRoleFuncs")
    @ResponseBody
    public String saveRoleFuncs(RoleFuncBO roleFuncBO){
        if(StringUtils.isEmpty(roleFuncBO.getRoleId()))
            return "error";
        try {
            Session session = SecurityUtils.getSubject().getSession();
            OperatorInfo operatorInfo = (OperatorInfo) session.getAttribute("operatorInfo");
            roleFuncBO.setLastUpdateOperator(operatorInfo.getOperatorName());
            roleFuncService.addRoleFuncs(roleFuncBO);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
            return "fail";
        }
    }



    @RequestMapping("/rolesWithSelected")
    @ResponseBody
    public List<RoleInfoBO> rolesWithSelected(int operatorId){
        List<RoleInfoBO> roleInfoBOList = roleInfoService.queryRoleListWithSelected(operatorId);
        return roleInfoBOList;
    }

    @RequestMapping(value = "/addRole")
    @ResponseBody
    public String add(RoleInfo roleInfo){
        try{
            Session session = SecurityUtils.getSubject().getSession();
            OperatorInfo operatorInfo = (OperatorInfo) session.getAttribute("operatorInfo");
            roleInfo.setLastUpdateOperator(operatorInfo.getOperatorName());
            roleInfoService.save(roleInfo);
            return "success";
        }catch (Exception e){
            e.printStackTrace();
            return "fail";
        }
    }


    @RequestMapping(value = "/deleteRole")
    @ResponseBody
    public String deleteRole(int id){
        try{
            Session session = SecurityUtils.getSubject().getSession();
            OperatorInfo operatorInfo = (OperatorInfo) session.getAttribute("operatorInfo");
            roleInfoService.delete(id,operatorInfo.getOperatorName());
            return "success";
        }catch (Exception e){
            e.printStackTrace();
            return "fail";
        }
    }
}
