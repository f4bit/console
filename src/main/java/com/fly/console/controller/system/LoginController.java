package com.fly.console.controller.system;

import com.fly.console.config.system.shiro.UsernamePasswordCustToken;
import com.fly.console.dao.OperatorInfo;
import com.fly.console.form.LoginForm;
import com.fly.console.util.CookieUtil;
import com.fly.console.util.constant.SystemConstant;
import com.fly.console.util.exception.IncorrectSystemException;
import com.google.code.kaptcha.Producer;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by APPLE on 2017/6/30.
 */
@Controller
public class LoginController {

    @Autowired
    private Producer captchaProducer;

    @Autowired
    RedisTemplate redisTemplate;

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String login(HttpServletRequest request) {
        return "/system/login";
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ModelAndView loginUser(@ModelAttribute("loginForm")LoginForm loginForm, HttpServletRequest httpServletRequest, ModelAndView modelAndView) {
        String captcha = loginForm.getCaptcha();
        Cookie cookie = CookieUtil.getCookieByName(httpServletRequest,"captchaCode");
        String uuid = cookie.getValue();
        String captchaBg = (String) redisTemplate.opsForValue().get(uuid);
        if(!captcha.equals(captchaBg)) {
            modelAndView.getModelMap().put("message", "验证码校验失败!");
            modelAndView.setViewName("/system/login");
            return modelAndView;
        }
        String remember = loginForm.getRemember();
        String username = loginForm.getUsername();
        String password = loginForm.getPassword();
        boolean rememberBoolean = true;
        if(StringUtils.isEmpty(remember)) {
            rememberBoolean = false;
        }
        String merLoginId = loginForm.getMerLoginId();
        if(StringUtils.isEmpty(merLoginId)) {
            merLoginId = SystemConstant.DEFAULT_MER_LOGIN_ID_KEY;
        }
        String host = httpServletRequest.getRemoteHost();

        Subject currentUser = SecurityUtils.getSubject();
        if (!currentUser.isAuthenticated()) {
            UsernamePasswordCustToken usernamePasswordCustToken = new UsernamePasswordCustToken(username, password,
                    rememberBoolean, host, merLoginId);
            try {
                currentUser.login(usernamePasswordCustToken);
            } catch (UnknownAccountException uae) {
                modelAndView.getModelMap().put("message", "用户账户不存在!");
                modelAndView.setViewName("/system/login");
            } catch (IncorrectCredentialsException ice) {
                modelAndView.getModelMap().put("message", "用户密码错误!");
                modelAndView.setViewName("/system/login");
            } catch (LockedAccountException lae) {
                modelAndView.getModelMap().put("message", "账户已关闭!");
                modelAndView.setViewName("/system/login");
            } catch (IncorrectSystemException ise) {
                modelAndView.getModelMap().put("message", ise.getMessage());
                modelAndView.setViewName("/system/login");
            } catch (AuthenticationException ae) {
                ae.printStackTrace();
                modelAndView.getModelMap().put("message", "认证异常!");
                modelAndView.setViewName("/system/login");
            }
        }else{//重复登录
            OperatorInfo operatorInfo = (OperatorInfo) currentUser.getPrincipal();
            if(!operatorInfo.getOperatorLoginId().equalsIgnoreCase(username)&&!operatorInfo.getMerLoginId().equalsIgnoreCase(merLoginId)){//如果登录名不同
                currentUser.logout();
                UsernamePasswordCustToken usernamePasswordCustToken = new UsernamePasswordCustToken(username, password,
                        true, host, merLoginId);
                try {
                    currentUser.login(usernamePasswordCustToken);
                } catch (UnknownAccountException uae) {
                    modelAndView.getModelMap().put("message", "用户账户不存在!");
                    modelAndView.setViewName("/system/login");
                } catch (IncorrectCredentialsException ice) {
                    modelAndView.getModelMap().put("message", "用户密码错误!");
                    modelAndView.setViewName("/system/login");
                } catch (LockedAccountException lae) {
                    modelAndView.getModelMap().put("message", "账户已关闭!");
                    modelAndView.setViewName("/system/login");
                } catch (IncorrectSystemException ise) {
                    modelAndView.getModelMap().put("message", ise.getMessage());
                    modelAndView.setViewName("/system/login");
                } catch (AuthenticationException ae) {
                    ae.printStackTrace();
                    modelAndView.getModelMap().put("message", "认证异常!");
                    modelAndView.setViewName("/system/login");
                }
            } else {
                modelAndView.setViewName("redirect:/system/home");
            }
        }
        return modelAndView;
    }

    @RequestMapping(value = "/logOut",method = RequestMethod.POST)
    public String logOut(HttpSession session) {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        session.removeAttribute("operatorCustInfo");
        return "login";
    }

    @RequestMapping(value = "/captcha-image")
    public ModelAndView getKaptchaImage(HttpServletRequest request,
                                        HttpServletResponse response) throws Exception {
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control",
                "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        response.setContentType("image/jpeg");

        String capText = captchaProducer.createText();
        System.out.println("capText: " + capText);

        try {
            //生成验证码
            String uuid = UUID.randomUUID().toString();
            redisTemplate.opsForValue().set(uuid, capText,60*5, TimeUnit.SECONDS);
            Cookie cookie = new Cookie("captchaCode",uuid);
            response.addCookie(cookie);
        } catch (Exception e) {
            e.printStackTrace();
        }

        BufferedImage bi = captchaProducer.createImage(capText);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(bi, "jpg", out);
        try {
            out.flush();
        } finally {
            out.close();
        }
        return null;
    }
}
