package com.fly.console.controller.system.auth;

import com.fly.console.config.system.shiro.ShiroService;
import com.fly.console.dao.FuncInfo;
import com.fly.console.dao.OperatorInfo;
import com.fly.console.service.FuncInfoService;
import com.fly.console.service.bo.FuncInfoBO;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by APPLE on 2017/7/13.
 */
@Controller
@RequestMapping("/system/auth/func_manager")
public class FuncController {

    @Autowired
    private FuncInfoService funcInfoService;

    @Autowired
    private ShiroService shiroService;

    @RequestMapping
    public String funcManager(HttpServletRequest request) {
        return "/system/auth/func";
    }


    @RequestMapping(value = "/funcs")
    @ResponseBody
    public Map func(FuncInfo funcInfo, String draw,
                    @RequestParam(required = false, defaultValue = "1") int start,
                    @RequestParam(required = false, defaultValue = "10") int length) {
        Map<String,Object> map = new HashMap<>();
        PageInfo<FuncInfo> pageInfo = funcInfoService.selectByPage(funcInfo, start, length);
        System.out.println("pageInfo.getTotal():"+pageInfo.getTotal());
        map.put("draw",draw);
        map.put("recordsTotal",pageInfo.getTotal());
        map.put("recordsFiltered",pageInfo.getTotal());
        map.put("data", pageInfo.getList());
        return map;
    }

    @RequestMapping("/funcsWithSelected")
    @ResponseBody
    public List<FuncInfoBO> funcsWithSelected(int roleId){
        List<FuncInfoBO> funcInfoBOList = new ArrayList<>();
        List<FuncInfoBO> funcInfoBOs = funcInfoService.queryFuncListWithSelected(roleId);
        for(FuncInfoBO funcInfoBO:funcInfoBOs) {
            funcInfoBO.setName(funcInfoBO.getFuncName());
            funcInfoBOList.add(funcInfoBO);
        }
        return funcInfoBOList;
    }

    @RequestMapping(value = "/addFunc")
    @ResponseBody
    public String add(FuncInfo funcInfo){
        try{
            Session session = SecurityUtils.getSubject().getSession();
            OperatorInfo operatorInfo = (OperatorInfo) session.getAttribute("operatorInfo");
            funcInfo.setLastUpdateOperator(operatorInfo.getOperatorName());
            funcInfoService.save(funcInfo);
            //更新权限
            shiroService.updatePermission();
            return "success";
        }catch (Exception e){
            e.printStackTrace();
            return "fail";
        }
    }

    @RequestMapping(value = "/deleteFunc")
    @ResponseBody
    public String deleteFunc(int id){
        try{
            Session session = SecurityUtils.getSubject().getSession();
            OperatorInfo operatorInfo = (OperatorInfo) session.getAttribute("operatorInfo");
            funcInfoService.delete(id,operatorInfo.getOperatorName());
            //更新权限
            shiroService.updatePermission();
            return "success";
        }catch (Exception e){
            e.printStackTrace();
            return "fail";
        }
    }


}
