package com.fly.console.controller.system.auth;

import com.fly.console.dao.OperatorInfo;
import com.fly.console.dao.TblMerInfo;
import com.fly.console.service.MerInfoService;
import com.fly.console.service.OperatorInfoService;
import com.fly.console.service.OperatorRoleService;
import com.fly.console.service.bo.OperatorRoleBO;
import com.fly.console.util.PasswordHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by APPLE on 2017/7/8.
 */
@Controller
@RequestMapping("/system/auth/operator_manager")
public class OperatorController {

    @Autowired
    private OperatorInfoService operatorInfoService;

    @Autowired
    private OperatorRoleService operatorRoleService;

    @Autowired
    private MerInfoService merInfoService;

    @RequestMapping
    public String operatorManager(Model model) {
        List<TblMerInfo> tblMerInfoList = merInfoService.queryAllNomarlMerInfoList();
        model.addAttribute("tblMerInfoList",tblMerInfoList);
        return "/system/auth/operator";
    }

    @RequestMapping(value = "/operators")
    @ResponseBody
    public Map operator(OperatorInfo operatorInfo, String draw,
                        @RequestParam(required = false, defaultValue = "1") int start,
                        @RequestParam(required = false, defaultValue = "10") int length) {
        Map<String,Object> map = new HashMap<>();
        PageInfo<OperatorInfo> pageInfo = operatorInfoService.selectByPage(operatorInfo, start, length);
        System.out.println("pageInfo.getTotal():"+pageInfo.getTotal());
        map.put("draw",draw);
        map.put("recordsTotal",pageInfo.getTotal());
        map.put("recordsFiltered",pageInfo.getTotal());
        map.put("data", pageInfo.getList());
        return map;
    }

    @RequestMapping(value = "/addOperator")
    @ResponseBody
    public String add(OperatorInfo operatorInfo){
        try{
            if(operatorInfoService.isExistOperator(operatorInfo)) {
                return "操作员已存在!";
            }
            Session session = SecurityUtils.getSubject().getSession();
            OperatorInfo operatorInfoSession = (OperatorInfo) session.getAttribute("operatorInfo");
            operatorInfo.setLastUpdateOperator(operatorInfoSession.getOperatorName());
            operatorInfoService.save(PasswordHelper.encryptPassword(operatorInfo));
            return "success";
        }catch (Exception e){
            e.printStackTrace();
            return "fail";
        }
    }


    @RequestMapping("/saveOperatorRoles")
    @ResponseBody
    public String saveOperatorRoles(OperatorRoleBO operatorRoleBO){
        if(StringUtils.isEmpty(operatorRoleBO))
            return "error";
        try {
            Session session = SecurityUtils.getSubject().getSession();
            OperatorInfo operatorInfo = (OperatorInfo) session.getAttribute("operatorInfo");
            operatorRoleBO.setLastUpdateOperator(operatorInfo.getOperatorName());
            operatorRoleService.addOperatorRole(operatorRoleBO);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
            return "fail";
        }
    }

    @RequestMapping(value = "/deleteOperator")
    @ResponseBody
    public String deleteOperator(int id){
        try{
            Session session = SecurityUtils.getSubject().getSession();
            OperatorInfo operatorInfo = (OperatorInfo) session.getAttribute("operatorInfo");
            operatorInfoService.delete(id,operatorInfo.getOperatorName());
            return "success";
        }catch (Exception e){
            e.printStackTrace();
            return "fail";
        }
    }






}
