package com.fly.console.controller.system;

import com.fly.console.dao.FuncInfo;
import com.fly.console.service.FuncInfoService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by APPLE on 2017/6/30.
 */
@Controller
public class HomeController {

    @Autowired
    FuncInfoService funcInfoService;

    @RequestMapping(value = "home")
    public String home(Model model) {
        int operatorId = (Integer) SecurityUtils.getSubject().getSession().getAttribute("operatorId");
        List<FuncInfo> funcInfoList = funcInfoService.queryMenuByOperatorId(operatorId);
        model.addAttribute("funcInfoList", funcInfoList);
        return "system/home";
    }

    @RequestMapping(value = "main")
    public String main() {
        return "system/main";
    }
}
