package com.fly.console.form;

/**
 * Created by APPLE on 2017/7/11.
 */
public class LoginForm {

    private String merLoginId;

    private String username;

    private String password;

    private String captcha;

    private String remember;

    public String getMerLoginId() {
        return merLoginId;
    }

    public void setMerLoginId(String merLoginId) {
        this.merLoginId = merLoginId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public String getRemember() {
        return remember;
    }

    public void setRemember(String remember) {
        this.remember = remember;
    }
}
