package com.fly.console.service;

import com.fly.console.dal.FuncInfoCustMapper;
import com.fly.console.dal.FuncInfoMapper;
import com.fly.console.dal.OperatorInfoCustMapper;
import com.fly.console.dao.FuncInfo;
import com.fly.console.dao.FuncInfoExample;
import com.fly.console.dao.RoleInfo;
import com.fly.console.dao.RoleInfoExample;
import com.fly.console.service.bo.FuncInfoBO;
import com.fly.console.util.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by APPLE on 2017/7/4.
 */
@Service
public class FuncInfoService {

    @Autowired
    private FuncInfoMapper funcInfoMapper;

    @Autowired
    private FuncInfoCustMapper funcInfoCustMapper;

    @Autowired
    private OperatorInfoCustMapper operatorInfoCustMapper;

    public List<FuncInfo> queryAll() {
        return funcInfoMapper.selectByExample(new FuncInfoExample());
    }

    public List<FuncInfo> queryMenuByOperatorId(int operatorId) {
        Map<String,Object> map = new HashMap<>();
        map.put("type",0);
        map.put("operatorId",operatorId);
        return operatorInfoCustMapper.queryMenuByOperatorId(map);
    }

    public List<FuncInfoBO> queryFuncListWithSelected(int roleId) {
        return funcInfoCustMapper.queryFuncListWithSelected(roleId);
    }

    public PageInfo<FuncInfo> selectByPage(FuncInfo funcInfo, int start, int length) {
        int page = start/length+1;
        FuncInfoExample funcInfoExample = new FuncInfoExample();
        FuncInfoExample.Criteria  funcInfoExampleCriteria = funcInfoExample.createCriteria();
        funcInfoExampleCriteria.andStatEqualTo(1);
        if (StringUtil.isNotEmpty(funcInfo.getFuncName())) {
            funcInfoExampleCriteria.andFuncNameLike("%" + funcInfo.getFuncName() + "%");
        }
        //分页查询
        PageHelper.startPage(page, length);
        List<FuncInfo> funcInfoList = funcInfoMapper.selectByExample(funcInfoExample);
        for(FuncInfo funcInfoResult:funcInfoList) {
            funcInfoResult.setUpdateTime(DateUtil.getDate(funcInfoResult.getUpdateTime(),DateUtil.DATE_TIME_PATTERN,DateUtil.TIME_PATTERN));
            funcInfoResult.setCreateTime(DateUtil.getDate(funcInfoResult.getCreateTime(),DateUtil.DATE_TIME_PATTERN,DateUtil.TIME_PATTERN));
        }
        return new PageInfo<>(funcInfoList);
    }

    public void save(FuncInfo funcInfo) {
        funcInfo.setStat(1);
        funcInfo.setUpdateTime(DateUtil.getDateAndTime());
        funcInfo.setCreateTime(DateUtil.getDateAndTime());
        funcInfoMapper.insert(funcInfo);
    }

    public void delete(int funcid,String lastUpdateOperator) {
        FuncInfo funcInfo = new FuncInfo();
        funcInfo.setStat(0);
        funcInfo.setFuncId(funcid);
        funcInfo.setLastUpdateOperator(lastUpdateOperator);
        funcInfo.setUpdateTime(DateUtil.getDateAndTime());
        funcInfoMapper.updateByPrimaryKeySelective(funcInfo);
    }
}
