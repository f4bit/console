package com.fly.console.service;

import com.fly.console.dal.OperatorRoleRelInfoMapper;
import com.fly.console.dao.OperatorRoleRelInfo;
import com.fly.console.dao.OperatorRoleRelInfoExample;
import com.fly.console.service.bo.OperatorRoleBO;
import com.fly.console.util.DateUtil;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.DateUtils;

/**
 * Created by APPLE on 2017/7/12.
 */
@Service
public class OperatorRoleService {

    @Autowired
    OperatorRoleRelInfoMapper operatorRoleRelInfoMapper;

    @Transactional(propagation= Propagation.REQUIRED,readOnly=false,rollbackFor={Exception.class})
    public void addOperatorRole(OperatorRoleBO operatorRoleBO) {
        OperatorRoleRelInfoExample operatorRoleRelInfoExample = new OperatorRoleRelInfoExample();
        OperatorRoleRelInfoExample.Criteria criteria = operatorRoleRelInfoExample.createCriteria();
        criteria.andOperatorIdEqualTo(operatorRoleBO.getOperatorId());
        operatorRoleRelInfoMapper.deleteByExample(operatorRoleRelInfoExample);
        //添加
        if(operatorRoleBO.getRoleId() != null) {
            String[] roleids = operatorRoleBO.getRoleId().split(",");
            for (String roleId : roleids) {
                OperatorRoleRelInfo operatorRoleRelInfo = new OperatorRoleRelInfo();
                operatorRoleRelInfo.setOperatorId(operatorRoleBO.getOperatorId());
                operatorRoleRelInfo.setRoleId(Integer.valueOf(roleId));
                operatorRoleRelInfo.setCreateTime(DateUtil.getDateAndTime());
                operatorRoleRelInfoMapper.insert(operatorRoleRelInfo);
            }
        }
    }

}
