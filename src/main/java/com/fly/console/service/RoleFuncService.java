package com.fly.console.service;

import com.fly.console.dal.OperatorRoleRelInfoMapper;
import com.fly.console.dal.RoleFuncRelInfoMapper;
import com.fly.console.dal.RoleInfoMapper;
import com.fly.console.dao.*;
import com.fly.console.service.bo.OperatorRoleBO;
import com.fly.console.service.bo.RoleFuncBO;
import com.fly.console.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by APPLE on 2017/7/12.
 */
@Service
public class RoleFuncService {

    @Autowired
    RoleFuncRelInfoMapper roleFuncRelInfoMapper;

    @Transactional(propagation= Propagation.REQUIRED,readOnly=false,rollbackFor={Exception.class})
    public void addRoleFuncs(RoleFuncBO roleFuncBO) {
        RoleFuncRelInfoExample roleFuncRelInfoExample = new RoleFuncRelInfoExample();
        RoleFuncRelInfoExample.Criteria criteria = roleFuncRelInfoExample.createCriteria();
        criteria.andRoleIdEqualTo(roleFuncBO.getRoleId());
        roleFuncRelInfoMapper.deleteByExample(roleFuncRelInfoExample);
        //添加
        if(roleFuncBO.getFuncId() != null) {
            String[] funcIds = roleFuncBO.getFuncId().split(",");
            for (String funcId : funcIds) {
                RoleFuncRelInfo roleFuncRelInfo = new RoleFuncRelInfo();
                roleFuncRelInfo.setRoleId(roleFuncBO.getRoleId());
                roleFuncRelInfo.setFuncId(Integer.valueOf(funcId));
                roleFuncRelInfo.setCreateTime(DateUtil.getDateAndTime());
                roleFuncRelInfoMapper.insert(roleFuncRelInfo);
            }
        }
    }

}
