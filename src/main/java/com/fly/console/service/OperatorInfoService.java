package com.fly.console.service;

import com.fly.console.dal.OperatorInfoCustMapper;
import com.fly.console.dal.OperatorInfoMapper;
import com.fly.console.dal.TblMerInfoMapper;
import com.fly.console.dao.*;
import com.fly.console.util.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by APPLE on 2017/7/4.
 */
@Service
public class OperatorInfoService {

    @Autowired
    private OperatorInfoCustMapper operatorInfoCustMapper;

    @Autowired
    private OperatorInfoMapper operatorInfoMapper;

    @Autowired
    private TblMerInfoMapper tblMerInfoMapper;

    public OperatorCustInfo findOperatorCustInfoByMerAndOperatorLoginId(String merLoginId,String operatorLoginId) {
        Map map = new HashMap<>();
        map.put("merLoginId",merLoginId);
        map.put("operatorLoginId",operatorLoginId);
        return operatorInfoCustMapper.findOperatorCustInfoByMerAndOperatorLoginId(map);
    }


    public OperatorInfo selectByOperatorAndMerLoginId(String merLoginId,String operatorLoginId) {
        OperatorInfoExample operatorInfoExample = new OperatorInfoExample();
        operatorInfoExample.createCriteria().andMerLoginIdEqualTo(merLoginId).andOperatorLoginIdEqualTo(operatorLoginId);
        List<OperatorInfo> operatorInfoList =  operatorInfoMapper.selectByExample(operatorInfoExample);
        if(operatorInfoList != null&& operatorInfoList.size() > 0) {
            return operatorInfoList.get(0);
        }
        return null;
    }



    public PageInfo<OperatorInfo> selectByPage(OperatorInfo operatorInfo, int start, int length) {
        int page = start/length+1;
        OperatorInfoExample operatorInfoExample = new OperatorInfoExample();
        OperatorInfoExample.Criteria  operatorInfoExampleCriteria = operatorInfoExample.createCriteria();
        if (StringUtil.isNotEmpty(operatorInfo.getOperatorLoginId())) {
            operatorInfoExampleCriteria.andOperatorLoginIdLike("%" + operatorInfo.getOperatorLoginId() + "%");
        }
        if (StringUtil.isNotEmpty(operatorInfo.getMerLoginId())) {
            operatorInfoExampleCriteria.andMerLoginIdEqualTo(operatorInfo.getMerLoginId());
        }
        if(operatorInfo.getStat() != null) {
            operatorInfoExampleCriteria.andStatEqualTo(operatorInfo.getStat());
        } else {
            operatorInfoExampleCriteria.andStatNotEqualTo(0);
        }
        //分页查询
        PageHelper.startPage(page, length);
        List<OperatorInfo> operatorInfoList = operatorInfoMapper.selectByExample(operatorInfoExample);
        for(OperatorInfo operatorInfoResult:operatorInfoList) {
            String merLoginId = operatorInfoResult.getMerLoginId();
            if(merLoginId.equals("console")) {
                operatorInfoResult.setMerLoginId("平台");
            } else {
                TblMerInfoExample tblMerInfoExample = new TblMerInfoExample();
                tblMerInfoExample.createCriteria().andMerLoginIdEqualTo(merLoginId);
                List<TblMerInfo> tblMerInfoList = tblMerInfoMapper.selectByExample(tblMerInfoExample);
                if(tblMerInfoList == null || tblMerInfoList.size() == 0) {
                    operatorInfoResult.setMerLoginId("未知");
                } else {
                    operatorInfoResult.setMerLoginId(tblMerInfoList.get(0).getMerName());
                }
            }
            operatorInfoResult.setUpdateTime(DateUtil.getDate(operatorInfoResult.getUpdateTime(),DateUtil.DATE_TIME_PATTERN,DateUtil.TIME_PATTERN));
            operatorInfoResult.setCreateTime(DateUtil.getDate(operatorInfoResult.getCreateTime(),DateUtil.DATE_TIME_PATTERN,DateUtil.TIME_PATTERN));
        }
        return new PageInfo<>(operatorInfoList);
    }


    public void save(OperatorInfo operatorInfo) {
        operatorInfo.setForcedUpdatePwdStat(0);
        operatorInfo.setLoginFailCount(0);
        operatorInfo.setStat(1);
        operatorInfo.setUpdateTime(DateUtil.getDateAndTime());
        operatorInfo.setCreateTime(DateUtil.getDateAndTime());
        operatorInfoMapper.insert(operatorInfo);
    }

    public boolean isExistOperator(OperatorInfo operatorInfo) {
        OperatorInfoExample operatorInfoExample = new OperatorInfoExample();
        operatorInfoExample.createCriteria().
                andMerLoginIdEqualTo(operatorInfo.getMerLoginId()).
                andOperatorLoginIdEqualTo(operatorInfo.getOperatorLoginId());
        List<OperatorInfo> operatorInfoList = operatorInfoMapper.selectByExample(operatorInfoExample);
        if(operatorInfoList != null && operatorInfoList.size() > 0) {
            return true;
        }
        return false;
    }

    public void delete(int operatorId,String lastUpdateOperator) {
        OperatorInfo operatorInfo = new OperatorInfo();
        operatorInfo.setStat(0);
        operatorInfo.setOperatorId(operatorId);
        operatorInfo.setLastUpdateOperator(lastUpdateOperator);
        operatorInfo.setUpdateTime(DateUtil.getDateAndTime());
        operatorInfoMapper.updateByPrimaryKeySelective(operatorInfo);
    }



}
