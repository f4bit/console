package com.fly.console.service;

import com.fly.console.dal.RoleInfoCustMapper;
import com.fly.console.dal.RoleInfoMapper;
import com.fly.console.dao.*;
import com.fly.console.service.bo.RoleInfoBO;
import com.fly.console.util.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by APPLE on 2017/7/12.
 */
@Service
public class RoleInfoService {

    @Autowired
    private RoleInfoCustMapper roleInfoCustMapper;

    @Autowired
    private RoleInfoMapper roleInfoMapper;

    public List<RoleInfoBO> queryRoleListWithSelected(int operatorId) {
        return roleInfoCustMapper.queryRoleListWithSelected(operatorId);
    }

    public PageInfo<RoleInfo> selectByPage(RoleInfo roleInfo, int start, int length) {
        int page = start/length+1;
        RoleInfoExample roleInfoExample = new RoleInfoExample();
        RoleInfoExample.Criteria  roleInfoExampleCriteria = roleInfoExample.createCriteria();
        roleInfoExampleCriteria.andStatEqualTo(1);
        if (StringUtil.isNotEmpty(roleInfo.getRoleName())) {
            roleInfoExampleCriteria.andRoleNameLike("%" + roleInfo.getRoleName() + "%");
        }
        //分页查询
        PageHelper.startPage(page, length);
        List<RoleInfo> roleInfoList = roleInfoMapper.selectByExample(roleInfoExample);
        for(RoleInfo roleInfoResult:roleInfoList) {
            roleInfoResult.setUpdateTime(DateUtil.getDate(roleInfoResult.getUpdateTime(),DateUtil.DATE_TIME_PATTERN,DateUtil.TIME_PATTERN));
            roleInfoResult.setCreateTime(DateUtil.getDate(roleInfoResult.getCreateTime(),DateUtil.DATE_TIME_PATTERN,DateUtil.TIME_PATTERN));
        }
        return new PageInfo<>(roleInfoList);
    }

    public void save(RoleInfo roleInfo) {
        roleInfo.setStat(1);
        roleInfo.setUpdateTime(DateUtil.getDateAndTime());
        roleInfo.setCreateTime(DateUtil.getDateAndTime());
        roleInfoMapper.insert(roleInfo);
    }

    public void delete(int roleId,String lastUpdateOperator) {
        RoleInfo roleInfo = new RoleInfo();
        roleInfo.setStat(0);
        roleInfo.setRoleId(roleId);
        roleInfo.setLastUpdateOperator(lastUpdateOperator);
        roleInfo.setUpdateTime(DateUtil.getDateAndTime());
        roleInfoMapper.updateByPrimaryKeySelective(roleInfo);
    }

}
