package com.fly.console.service;


import com.fly.console.dal.TblMerInfoMapper;
import com.fly.console.dao.TblMerInfo;
import com.fly.console.dao.TblMerInfoExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by APPLE on 2017/7/13.
 */
@Service
public class MerInfoService {

    @Autowired
    private TblMerInfoMapper tblMerInfoMapper;

    public List<TblMerInfo> queryAllNomarlMerInfoList() {
        TblMerInfoExample tblMerInfoExample = new TblMerInfoExample();
        tblMerInfoExample.createCriteria().andMerStatEqualTo("0");
        return tblMerInfoMapper.selectByExample(tblMerInfoExample);
    }

}
