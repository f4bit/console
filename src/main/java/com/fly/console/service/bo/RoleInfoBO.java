package com.fly.console.service.bo;

import com.fly.console.dao.RoleInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by APPLE on 2017/7/12.
 */
@Setter
@Getter
public class RoleInfoBO {

    private Integer roleId;

    private String roleName;

    private String roleDesc;

    private Integer stat;

    private String lastUpdateOperator;

    private String updateTime;

    private String createTime;

    private Integer selected;

}
