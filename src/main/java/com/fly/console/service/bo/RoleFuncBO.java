package com.fly.console.service.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by APPLE on 2017/7/12.
 */
@Setter
@Getter
public class RoleFuncBO {

    private Integer roleId;

    private String funcId;

    private String lastUpdateOperator;

}
