package com.fly.console.service.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by APPLE on 2017/7/12.
 */
@Setter
@Getter
public class OperatorRoleBO {

    private Integer operatorId;

    private String roleId;

    private String lastUpdateOperator;

}
