package com.fly.console.service.bo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FuncInfoBO {

    private Integer funcId;

    private String funcName;

    private String funcDesc;

    private Integer type;

    private Integer funcLevel;

    private Integer funcOrder;

    private String funcIcon;

    private Integer isLeaf;

    private String funcUrl;

    private Integer parentFuncId;

    private Integer stat;

    private String lastUpdateOperator;

    private String updateTime;

    private String createTime;

    private String checked;

    private String name;

}