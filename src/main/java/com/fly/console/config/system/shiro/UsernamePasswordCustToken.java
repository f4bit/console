package com.fly.console.config.system.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * Created by APPLE on 2017/7/11.
 */
public class UsernamePasswordCustToken extends UsernamePasswordToken {

    private String merLoginId;

    public String getMerLoginId() {
        return merLoginId;
    }

    public void setMerLoginId(String merLoginId) {
        this.merLoginId = merLoginId;
    }

    public UsernamePasswordCustToken() {
        super();
    }

    public UsernamePasswordCustToken(String username, String password,
                                       boolean rememberMe, String host,String merLoginId) {
        super(username, password, rememberMe, host);
        this.merLoginId = merLoginId;
    }
}
