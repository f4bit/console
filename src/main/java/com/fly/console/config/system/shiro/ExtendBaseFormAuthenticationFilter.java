package com.fly.console.config.system.shiro;


import com.fly.console.util.constant.SessionKeyConstant;
import com.fly.console.util.constant.SystemConstant;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.context.annotation.Bean;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


public class ExtendBaseFormAuthenticationFilter extends BaseFormAuthenticationFilter {


	private String merLoginIdKey = SessionKeyConstant.MER_LOGIN_ID_KEY;

	public String getMerLoginIdKey() {
		return merLoginIdKey;
	}

	protected String getMerLoginId(ServletRequest request) {
		return WebUtils.getCleanParam(request, getMerLoginIdKey());
	}


	@Override
	protected AuthenticationToken createToken(ServletRequest request,
											  ServletResponse response) {
		String username = getUsername(request);
		String password = getPassword(request);
		String merLoginId = getMerLoginId(request);

		if(StringUtils.isEmpty(merLoginId)) {
			merLoginId = SystemConstant.DEFAULT_MER_LOGIN_ID_KEY;
		}

		boolean rememberMe = isRememberMe(request);
		String host = getHost(request);
		return new UsernamePasswordCustToken(username, password, rememberMe,
				host, merLoginId);
	}

}
