package com.fly.console.config.system.shiro;

import com.fly.console.dao.FuncInfo;
import com.fly.console.dao.OperatorCustInfo;
import com.fly.console.dao.OperatorInfo;
import com.fly.console.dao.RoleCustInfo;
import com.fly.console.service.FuncInfoService;
import com.fly.console.service.OperatorInfoService;
import com.fly.console.util.CookieUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.apache.shiro.web.util.WebUtils;

import javax.annotation.Resource;
import java.util.Set;

public class MyShiroRealm extends AuthorizingRealm {

    @Resource
    private OperatorInfoService operatorInfoService;

    @Resource
    private FuncInfoService funcInfoService;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        OperatorInfo operatorInfo= (OperatorInfo) SecurityUtils.getSubject().getPrincipal();

        OperatorCustInfo operatorCustInfo = operatorInfoService.findOperatorCustInfoByMerAndOperatorLoginId(operatorInfo.getMerLoginId(),operatorInfo.getOperatorLoginId());
        // 权限信息对象info,用来存放查出的用户的所有的角色（role）及权限（permission）
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        Set<RoleCustInfo> roleCustInfoSet = operatorCustInfo.getRoleCustInfoSet();
        for(RoleCustInfo roleCustInfo: roleCustInfoSet){
            info.addRole(roleCustInfo.getRoleName());
            Set<FuncInfo> funcInfoSet = roleCustInfo.getFuncInfoSet();
            for(FuncInfo funcInfo:funcInfoSet) {
                info.addStringPermission(funcInfo.getFuncUrl());
            }
        }
        return info;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        UsernamePasswordCustToken usernamePasswordCustToken = (UsernamePasswordCustToken) token;

        String merLoginId = usernamePasswordCustToken.getMerLoginId();
        String operatorLoginId = usernamePasswordCustToken.getUsername();
        OperatorInfo operatorInfo = operatorInfoService.selectByOperatorAndMerLoginId (merLoginId,operatorLoginId);
        if(operatorInfo==null) throw new UnknownAccountException();
        if (2==operatorInfo.getStat()) {
            throw new LockedAccountException(); // 帐号锁定
        }
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                operatorInfo, //用户
                operatorInfo.getOperatorLoginPwd(), //密码
                ByteSource.Util.bytes(operatorInfo.getOperatorLoginId()),
                getName()  //realm name
        );
        // 当验证都通过后，把用户信息放在session里
        Session session = SecurityUtils.getSubject().getSession();
        session.setAttribute("operatorInfo", operatorInfo);
        session.setAttribute("operatorId", operatorInfo.getOperatorId());
        return authenticationInfo;
    }

    /**
     * 指定principalCollection 清除
     */
  /*  public void clearCachedAuthorizationInfo(PrincipalCollection principalCollection) {

        SimplePrincipalCollection principals = new SimplePrincipalCollection(
                principalCollection, getName());
        super.clearCachedAuthorizationInfo(principals);
    }
*/
}
