package com.fly.console.util.exception;

import org.apache.shiro.authc.AuthenticationException;

/**
 * Created by APPLE on 2016/12/1.
 */
public class IncorrectSystemException extends AuthenticationException {

    public IncorrectSystemException() {
        super();
    }

    public IncorrectSystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectSystemException(String message) {
        super(message);
    }

    public IncorrectSystemException(Throwable cause) {
        super(cause);
    }
}
