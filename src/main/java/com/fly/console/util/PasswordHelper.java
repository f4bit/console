package com.fly.console.util;


import com.fly.console.dao.OperatorInfo;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public class PasswordHelper {
	//private RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
	private static String algorithmName = "md5";
	private static int hashIterations = 2;

	public static OperatorInfo encryptPassword(OperatorInfo operatorInfo) {
		//String salt=randomNumberGenerator.nextBytes().toHex();
		String newPassword = new SimpleHash(algorithmName, operatorInfo.getOperatorLoginPwd(), ByteSource.Util.bytes(operatorInfo.getOperatorLoginId()), hashIterations).toHex();
		//String newPassword = new SimpleHash(algorithmName, user.getPassword()).toHex();
		operatorInfo.setOperatorLoginPwd(newPassword);
		return operatorInfo;
	}
}
