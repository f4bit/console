package com.fly.console.util.constant;

/**
 * Created by APPLE on 2016/12/1.
 */
public class SessionKeyConstant {

    public static final String CAPTCHA_KEY = "captcha";

    public static final String MER_LOGIN_ID_KEY = "merLoginId";

}
